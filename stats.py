import re
import math
import collections
import sys

players = []
averages = {}

class Player:
	def __init__(self, name, bat, hits):
                self.name = name
                self.bat = bat
                self.hits = hits

def find(name):
    for p in players:
        if p.name == name:
            return p            

if len(sys.argv) != 2:
	sys.exit("Program Usage: %s [Input file]" % sys.argv[0])
 
input_file = sys.argv[1]
 

f = open(input_file, "r")

line = f.readline()

while line:
	expr = '(?P<name>[\w\s]+)\sbatted\s(?P<bat>\d)[\w\s]+with\s(?P<hits>\d)'
	match = re.match(expr,line)
	if match:
		name = match.group('name')
		bat = int(match.group('bat'))
		hits = int(match.group('hits'))
		player = find(name)
		if player is None:
			players.append(Player(name, bat, hits))
		else:
			player.bat += bat
			player.hits += hits
	line = f.readline()

f.close()

for p in players:
	avg = round(float(p.hits) / p.bat, 3)
	averages[p.name] = avg

Player = collections.namedtuple('Player', 'average')
leaders = sorted([Player(v,k) for (k,v) in averages.items()], reverse=True)

for p in leaders:
	print p[1],':',"%.3f" %p[0]